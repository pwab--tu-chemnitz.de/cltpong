extends KinematicBody2D

const SPEED = 100000

signal position_changed
var velocity = Vector2(0,0)
var last_position = Vector2(0,0)
onready var mouse_position = self.position
var distance = 0

func _input(event):
	if event is InputEventMouseMotion:
		mouse_position = get_global_mouse_position()


func _process(delta):
	distance = mouse_position - self.position
	if distance.length() > 15:
		velocity = distance.normalized() * SPEED
	else:
		velocity = Vector2(0,0)
	
#	velocity = Vector2(0,0)
#
#	if Input.is_action_pressed("left"):
#		velocity += Vector2.LEFT * SPEED
#	elif Input.is_action_pressed("right"):
#		velocity += Vector2.RIGHT * SPEED
#
#	if Input.is_action_pressed("up"):
#		velocity += Vector2.UP * SPEED
#	elif Input.is_action_pressed("down"):
#		velocity += Vector2.DOWN * SPEED
	
	move_and_slide(velocity * delta)
	
	if last_position != self.position:
		emit_signal("position_changed")
		
	last_position = self.position
