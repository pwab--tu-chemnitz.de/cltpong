extends Node

# For testing purposes
const TEST_MODE = false

# Our WebSocketClient instance.
var _client = WebSocketClient.new()

# Player identificator
var room = ""
var id = ""

# Commandline arguments
var args = {
	"url" : "wss://bbb.live.linux-tage.de/cltpong"
}

# Playernodes
const player_script = preload("res://player/player.gd")
const player_node = preload("res://player/player.tscn")
const ball_node = preload("res://ball/ball.tscn")
var player_instance
var enemy_instance
var ball_instance

# Stuff for the game
var game_state = {}

# Server communication
enum REQUEST {ECHO, ROOM, POSITIONCHANGE, COLLISION}
enum PAKETS {ECHO, HELLO, ROOM, PLAYERCONNECTED, PLAYERDISCONNECTED, GAMESTATE, GAMESTART, GAMEEND}


func _get_url_parameter():
	# Check url parameter for room entrance
	if OS.has_feature('JavaScript'):
		var url = str(JavaScript.eval("window.location.href")).split("#")
		if url.size() > 1:
			var parameter = {
				"url" : url[0],
				"room" : url[1]
			}
			return parameter
		else:
			print("Strange URL (Roomlink wrong?)")


func _init():
	if TEST_MODE:
		args["url"] = "ws://localhost:9080"
	
	# Read the arguments
	for argument in OS.get_cmdline_args():
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			args[key_value[0].lstrip("--")] = key_value[1]
	print("Arguments: ", args)


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	_client.connect("data_received", self, "_on_data")
	
	# Initiate connection to the given URL.
	var err = _client.connect_to_url(args["url"])
	if err != OK:
		print("Unable to connect")
		set_process(false)


func _closed(was_clean = false):
	print("Closed, clean: ", was_clean)
	set_process(false)


func _connected(proto = ""):
	print("Connected to server")
	open_room()


func open_room():
	# Read the url for entering a specific room
	print("Now opening room")
	
	if TEST_MODE:
		send_data(str(REQUEST.ROOM) + "|" + "" + "|" + "1")
		return
	
	var url_parameter = _get_url_parameter()
	if url_parameter != null:
		var room_id = str(url_parameter["room"])
		print("Trying to enter room: ", room_id)
		send_data(str(REQUEST.ROOM) + "|" + "" + "|" + room_id)


func _on_room_entered(pos, col):
	player_instance = player_node.instance()
	player_instance.name = id
	player_instance.position = str2vec(pos)
	player_instance.modulate = Color(col)
	player_instance.set_script(player_script)
	add_child(player_instance)
	player_instance.connect("position_changed", self, "_on_position_changed")
	player_instance.get_node("Area2D").connect("body_entered", self, "_on_collide")


func _on_position_changed():
	send_data(str(REQUEST.POSITIONCHANGE) + "|" + room + "|" + str(player_instance.position))


func _on_collide(body):
	if body.name == "ball":
		send_data(str(REQUEST.COLLISION) + "|"  + room + "|" + "")


func str2vec(vector2string):
	var pos_array = str(vector2string).trim_prefix("(").trim_suffix(")").replace(" ", "").split(",")
	return Vector2(pos_array[0], pos_array[1])


func _on_data():
	# EVERY PAKET IS DIVIDED INTO
	# TYPE | DATA
	var data = _client.get_peer(1).get_packet().get_string_from_utf8().split("|")
	
	# Based on data type do something
	if int(data[0]) == PAKETS.ECHO:
		print("ECHO: ", data[1])
		
	elif int(data[0]) == PAKETS.HELLO:
		id = data[1]
		print("I am Player ", id)
		
	elif int(data[0]) == PAKETS.ROOM:
		room = data[1] # Room or null
		if room == "":
			print(":(")
		else:
			print("Now in room ", room)
			_on_room_entered(data[2], data[3])
		
	elif int(data[0]) == PAKETS.GAMESTART:
		print("Game has started")
		ball_instance = ball_node.instance()
		ball_instance.position = str2vec(data[1])
		add_child(ball_instance)
	
	elif int(data[0]) == PAKETS.GAMEEND:
		print("Game has ended")
		ball_instance.queue_free()
	
	elif int(data[0]) == PAKETS.PLAYERCONNECTED:
		print("Other player connected")
		enemy_instance = player_node.instance()
		enemy_instance.modulate = Color(data[3])
		enemy_instance.name = data[1]
		enemy_instance.position = str2vec(data[2])
		add_child(enemy_instance)
	
	elif int(data[0]) == PAKETS.PLAYERDISCONNECTED:
		print("Other player disconnected")
		enemy_instance.hide()
		enemy_instance.queue_free()
	
	elif int(data[0]) == PAKETS.GAMESTATE:
		print("Gamestate has been updated")
		game_state = str2var(data[1])
		update_game()

# TIMESTAMP DELTATIME
#	elif data[1] == "1": # Deltatime
#		var now = OS.get_system_time_msecs()
#		var delta_time = now - int(data[2])
#		print("Then: ", data[2])
#		print("Now : ", now)
#		print("Latenz: ", delta_time)
#		print("-----------------")

func update_game():
	# Take the game_state and do something with it
	
	# Calculate Collisions
	# LOL
	
	# Update position of the ball
	if game_state["game"]["state"] == "running":
		get_node("ball").position = str2vec(game_state["ball"]["position"])
	
	# Update positions of the players
	for player_id in game_state["players"].keys():
		#### TODO ####
		get_node(str(player_id)).position = str2vec(game_state["players"][player_id]["position"])
	
	# Update time and points


#func _input(event):
#	# TESTING: If hit enter send echo request with timestamp
#	if event.is_action_pressed("info"):
#		send_data(str(REQUEST.ECHO) + "|" + room + "|" + str(OS.get_system_time_msecs()))


func send_data(data):
	_client.get_peer(1).put_packet(data.to_utf8())


func _process(_delta):
	# Data update every frame
	_client.poll()


func _exit_tree():
	_client.disconnect_from_host()
