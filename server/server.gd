extends Node

# Commandline arguments
var args = {
	"port" : "9080"
}

# Our WebSocketServer instance.
var _server = WebSocketServer.new()

# Player Counter
var current_players = 0

# Our room list
var room_data = {}

# Our standard game data
var standard_game_state = {
	"game" : {
		"state" : "not_running",
		"time" : 0,
		"points_left" : 0,
		"points_right" : 0
	},
	"ball" : {
		"position" : Vector2(480,540),
		"linear_velocity" : Vector2(0,0),
		"angular_velocity" : Vector2(0,0)
	},
	"players" : {}
}

const BALL_SPEED = 500

var available_positions = [Vector2(480,980), Vector2(480,100)]
var available_normals = [Vector2(0,-1), Vector2(0,1)]
var available_colors = ["#2a9df4", "#ff7f7f"]

enum REQUEST {ECHO, ROOM, POSITIONCHANGE, COLLISION}
enum PAKETS {ECHO, HELLO, ROOM, PLAYERCONNECTED, PLAYERDISCONNECTED, GAMESTATE, GAMESTART, GAMEEND}


func _init():
	# Read the command line arguments
	for argument in OS.get_cmdline_args():
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			args[key_value[0].lstrip("--")] = key_value[1]
	print("Arguments: ", args)


func _ready():
	_server.connect("client_connected", self, "_connected")
	_server.connect("client_disconnected", self, "_disconnected")
	_server.connect("client_close_request", self, "_close_request")
	_server.connect("data_received", self, "_on_data")
	
	# Start listening on the given port.
	var err = _server.listen(int(args["port"]))
	if err != OK:
		print("Unable to start server")
		set_process(false)


func _connected(id, proto):
	print("Client %d connected" % [id])
	current_players += 1
	send_to_player(id, str(PAKETS.HELLO) + "|" + str(id))


func move_to_room(id, room):
	# Create room if it doesn't exist
	if not room_data.has(room):
		room_data[room] = standard_game_state.duplicate(true)
	
	# Look into the room and let the player enter or not
	var game_state = room_data[room]
	if game_state["players"].size() < 2:
		game_state["players"][id] = {
			"position": available_positions.pop_front(),
			"normal": available_normals.pop_front(),
			"color": available_colors.pop_front()
		}
		send_to_player(id, str(PAKETS.ROOM) + "|" + room + "|" + str(game_state["players"][id]["position"]) + "|" +  str(game_state["players"][id]["color"]))
		
		# Now there could be 2 players
		if game_state["players"].size() == 2:
			var ids = game_state["players"].keys()
			send_to_player(ids[0], str(PAKETS.PLAYERCONNECTED) + "|" + str(ids[1]) + "|" + str(game_state["players"][ids[1]]["position"]) + "|" +  str(game_state["players"][ids[1]]["color"]))
			send_to_player(ids[1], str(PAKETS.PLAYERCONNECTED) + "|" + str(ids[0]) + "|" + str(game_state["players"][ids[0]]["position"]) + "|" +  str(game_state["players"][ids[0]]["color"]))
			start_game(room)
			
		publish_game_state(room)
	else:
		# Room is full - Send sorry to the player
		send_to_player(id, str(PAKETS.ROOM) + "|" + "")


func _close_request(id, code, reason):
	print("Client %d disconnecting with code: %d, reason: %s" % [id, code, reason])


func _disconnected(id, was_clean = false):
	print("Client %d disconnected, clean: %s" % [id, str(was_clean)])
	current_players -= 1
	
	# Go through the rooms to update the game_state
	for room in room_data.keys():
		if room_data[room]["players"].has(id):
			available_positions.append(room_data[room]["players"][id]["position"])
			available_normals.append(room_data[room]["players"][id]["normal"])
			available_colors.append(room_data[room]["players"][id]["color"])
			room_data[room]["players"].erase(id)
			end_game(room)
		
		for player_id in room_data[room]["players"].keys():
			send_to_player(id, str(PAKETS.PLAYERDISCONNECTED) + "|")
			publish_game_state(room)


func _on_data(id):
	# EVERY PAKET IS DIVIDED INTO
	# REQUEST | ROOM | DATA
	
	# Some data has been received
	var packet = _server.get_peer(id).get_packet()
	var data = packet.get_string_from_utf8().split("|")
	#print("Got data from client %d: %s" % [id, data])
	
	# Based on data type do something
	if int(data[0]) == REQUEST.ECHO:
		_server.get_peer(id).put_packet(packet)
	elif int(data[0]) == REQUEST.ROOM:
		move_to_room(id, data[2])
	elif int(data[0]) == REQUEST.POSITIONCHANGE:
		room_data[data[1]]["players"][id]["position"] = str2var(data[2])
		publish_game_state(data[1])
	elif int(data[0]) == REQUEST.COLLISION:
		var vel = room_data[data[1]]["ball"]["linear_velocity"]
		room_data[data[1]]["ball"]["linear_velocity"] = vel.bounce(room_data[data[1]]["players"][id]["normal"])
		room_data[data[1]]["ball"]["position"] += room_data[data[1]]["ball"]["linear_velocity"].normalized()
		publish_game_state(data[1])


func start_game(room):
	room_data[room]["ball"]["linear_velocity"] = BALL_SPEED * Vector2(randf(), randf())
	room_data[room]["game"]["state"] = "running"
	# start timer
	send_to_both_players(room, str(PAKETS.GAMESTART) + "|" + str(room_data[room]["ball"]["linear_velocity"]))
	publish_game_state(room)


func end_game(room):
	room_data[room]["game"] = {
		"state" : "not_running",
		"time"	: 0,
		"points_left" : 0,
		"points_right" : 0
	}
	room_data[room]["ball"] = {
		"position" : Vector2(480,540),
		"linear_velocity" : Vector2(0,0),
		"angular_velocity" : Vector2(0,0)
	}
	
	send_to_both_players(room, str(PAKETS.GAMEEND) + "|" + "")


func send_to_player(id, data):
	_server.get_peer(id).put_packet(data.to_utf8())


func send_to_both_players(room, data):
	for id in room_data[room]["players"]:
		_server.get_peer(id).put_packet(data.to_utf8())


func publish_game_state(room):
	var game_state = room_data[room]
	send_to_both_players(room, str(PAKETS.GAMESTATE) + "|" + var2str(game_state))


func publish_to_all():
	pass


func _process(_delta):
	# Data transfer, and signals emission will only happen when calling this function.
	_server.poll()
	
	for room in room_data.keys():
		if room_data[room]["game"]["state"] == "running":
			# Caculate ball positions
			room_data[room]["ball"]["position"] += room_data[room]["ball"]["linear_velocity"] * _delta
			
			# Wall collisions
			if room_data[room]["ball"]["position"].x <= 20:
				room_data[room]["ball"]["linear_velocity"].x *= -1
				room_data[room]["ball"]["position"].x += 1
			elif room_data[room]["ball"]["position"].x >= 940:
				room_data[room]["ball"]["linear_velocity"].x *= -1
				room_data[room]["ball"]["position"].x -= 1
			
			if room_data[room]["ball"]["position"].y <= 20:
				room_data[room]["ball"]["linear_velocity"].y *= -1
				room_data[room]["ball"]["position"].x += 1
			elif room_data[room]["ball"]["position"].y >= 1060:
				room_data[room]["ball"]["linear_velocity"].y *= -1
				room_data[room]["ball"]["position"].y -= 1
			
			publish_game_state(room)
			


func _exit_tree():
	_server.stop()
