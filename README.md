# cltpong

Ein Multiplayer Pong für die CLT2021

## Projektstruktur

- Server für Linuxsystem
- Client für Windows, Linux und HTML5

## How To

### Server

1. In den Ordner `server/builds` wechseln
2. Starten des Servers mit dem Binary gefolgt von einem Argument für den Port
	- `godot pong_server.86_64 --port=9080`
	- Falls die erste Zeile nicht geht lässt sich auch (irgendwie) nur  das Package laden: `./Godot_v3.2-stable_linux_server.64 --main-pack pong_server.pck --port=9080`
3. Laufen lassen und Output beobachten

### Client

1. In den Ordner `client/builds` wechseln
2. Starten des Clients mit dem Binary gefolgt von einem Argument mit der URL (inklusive Port)
	- `pong_client --url=ws://localhost:9080`